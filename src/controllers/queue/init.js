'use strict'
//* Модуль инициализации задач для ТВ

import { Op } from 'sequelize'

export default async function (fastify, deviceId) {
  const now = new Date()
  const dateCheck = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate()

  // Поиск сессий привязанных к deviceId
  const Session = fastify.db.models.Session
  const session = await Session.findOne({
    where: {
      deviceId: deviceId,
      mode: 0,
      [Op.and]: [
        fastify.db.where(fastify.db.fn('date', fastify.db.col('creationTime')), '>=', dateCheck)
      ]
    },
    order: [ [ 'id' ] ]
  })

  if (session === null) return false

  return session.dataValues.sessionId
}