'use strict'
//* Модуль создания новой игровой сессии

export default async function (fastify, deviceId, sessionId, sessionTime) {
  // Поиск deviceId
  const Device = fastify.db.models.Device
  const device = await Device.findByPk(deviceId)
  
  if (device === null) return 1

  // Проверка устройства на доступность
  if (!global['devicesStatus'].find(x => x.id === deviceId).connected) return 2

  // Поиск sessionId
  const Session = fastify.db.models.Session
  const session = await Session.findOne({ where: { sessionId: sessionId } })

  if (session !== null) return 3

  // Создание сессии
  try {
    await Session.create({
      sessionId: sessionId,
      deviceId: deviceId,
      time: sessionTime,
      mode: 0
    })
  } catch (error) {
    fastify.log.error('An error occurred while creating the session:', error )
    return 4
  }

  // Проверить очередь и вывести QR если очередь пустая
  try {
    await fastify.tvCheckQueue(deviceId)
  } catch (error) {
    return  5
  }

  return 0
}