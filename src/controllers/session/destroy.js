'use strict'
//* Модуль удаления игровой сессии

export default async function (fastify, sessionId) {
  // Поиск sessionId
  const Session = fastify.db.models.Session
  const session = await Session.findOne({ where: { sessionId: sessionId } })

  if (session === null) return 1

  // Проверка режима отображения
  if (session.dataValues.mode !== 1) return 2

  // Запуск игры
  try {
    fastify.tvStartGame(sessionId, session.dataValues.deviceId)
  } catch (error) {
    fastify.log.error('An error occurred while device switching at game mode:', error )
    return 9
  }

  return 0
}