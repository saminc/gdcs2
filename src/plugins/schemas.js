'use strict'

import fp from 'fastify-plugin'

export default fp(async function (fastify, opts) {
  fastify.addSchema({
    $id: 'schemaDeviceId',
    description: 'Уникальный ID игрового устройства',
    type: 'integer',
    format: 'int32',
    minimum: 1,
    maximum: 2147483647
  })

  fastify.addSchema({
    $id: 'schemaDeviceQueue',
    description: 'Текущая очередь на данном устройстве, 0 если очередь пустая',
    type: 'integer',
    format: 'int32',
    minimum: 0,
    maximum: 2147483647
  })

  fastify.addSchema({
    $id: 'schemaSessionId',
    description: 'Уникальный ID игровой сессии',
    type: 'integer',
    format: 'int32',
    minimum: 1,
    maximum: 2147483647
  })

  fastify.addSchema({
    $id: 'schemaSessionTime',
    description: 'Время игровой сессии в секундах',
    type: 'integer',
    format: 'int32',
    minimum: 60,
    maximum: 1440
  })
})