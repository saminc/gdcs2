'use strict'

import fp from 'fastify-plugin'
import Sequelize from 'sequelize'
import configDB from '../../db/config.js'
import { modelCityInit } from '../models/city.js'
import { modelBranchInit } from '../models/branch.js'
import { modelDeviceInit } from '../models/device.js'
import { modelSessionInit } from '../models/session.js'

export default fp(async (fastify, opts) => {
  const sequelize = new Sequelize(configDB[process.env.APP_ENV])

  try {
    await sequelize.authenticate()
    fastify.log.info('Connection to DB has been established successfully.')
  } catch (error) {
    throw new Error('Unable to connect to the database:' + error)
  }

  try {
    const City = await modelCityInit(sequelize, Sequelize.DataTypes)
    const Branch = await modelBranchInit(sequelize, Sequelize.DataTypes)

    Branch.hasOne(City, {
      foreignKey: 'id',
      sourceKey: 'cityId',
      onDelete: 'cascade'
    })

    const Device = await modelDeviceInit(sequelize, Sequelize.DataTypes)

    Device.hasOne(Branch, {
      foreignKey: 'id',
      sourceKey: 'branchId',
      onDelete: 'cascade'
    })

    const Session = await modelSessionInit(sequelize, Sequelize.DataTypes, Sequelize)

    Session.hasOne(Device, {
      foreignKey: 'id',
      sourceKey: 'deviceId',
      onDelete: 'cascade'
    })

    fastify.log.info('All models loaded.')
  } catch (error) {
    throw new Error('Error load models:' + error)
  }

  await sequelize.sync().then(() => {
    fastify.log.info('All tables sync successfully!')
  }).catch((error) => {
    throw new Error('Unable to sync table : ' + error)
  })

  fastify.decorate('db', sequelize)
  fastify.addHook('onClose', (fastifyInstance, done) => {
    fastify.log.info('Connection to DB has been closed.')
    sequelize
      .close()
      .finally(done)
  })
})