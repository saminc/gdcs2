'use strict'

import fp from 'fastify-plugin'

export default fp(async function (fastify, opts) {
  fastify.decorate('authenticate', async function (request, reply) {
    try {
      const apiKey = request.headers['x-api-key'] || ''
      if (apiKey !== process.env.SECURITY_API_KEY) {
        reply.code(401).send('Unauthorized')
      }
    } catch (err) {
      reply.send(err)
    }
  })

  fastify.decorate('auth', async function (request, reply) {
    const apiKey = request.headers['x-api-key'] || ''
    console.log(apiKey)
  })
})