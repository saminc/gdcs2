'use strict'
//* Модуль работы с ТВ

import fp from 'fastify-plugin'
import { unlink } from 'node:fs/promises'
import lgtv from 'lgtv2'
import QRCode from 'qrcode'
import initTV from '../controllers/queue/init.js'

export default fp(async (fastify, opts) => {
  global['devicesStatus'] = []
  const Device = fastify.db.models.Device
  const devices = await Device.findAll()

  Object.keys(devices).forEach(function(key, index) {
    if (!devices[key].dataValues.disable) {
      let deviceId = devices[key].dataValues.id
      let deviceProps = {
        id: deviceId,
        ipTV: devices[key].dataValues.ipTV,
        connected: false
      }

      global['devicesStatus'].push(deviceProps)

      let tvId = `tv${deviceId}`
      global[tvId] = lgtv({ url: `ws://${deviceProps.ipTV}:3000` })

      global[tvId].on('error', function (err) {
        fastify.log.error(`Device[${deviceId}] connected error`)
        fastify.log.error(err)
      })

      global[tvId].on('connect', function () {
        fastify.log.info(`Device[${deviceId}] connected`)

        global['devicesStatus'].find(device => device.id === deviceId).connected = true
        console.log(devicesStatus)
  
        global[tvId].subscribe('ssap://audio/getVolume', function (err, res) {
          if (res.changed.indexOf('volume') !== -1) fastify.log.info(`TV[${deviceId}] volume changed`, res.volume)
          if (res.changed.indexOf('muted') !== -1) fastify.log.info('mute changed', res.muted)
        })

        initTV(fastify, deviceId).then(
          function (sessionId) {
            if (sessionId === false) {
              const tvTimerAD = `tvTimerAD${deviceId}`
              const interval = process.env.AD_DURATION
              global[tvTimerAD] = setInterval(showAd, interval, deviceId)
          
              showAd(deviceId)
            } else {
              generateQR(sessionId).then(
                function (result) {
                  showQR(deviceId, sessionId)
                },
                function (err) {
                  fastify.log.error('QR code generation error:', err)
                }
              )
            }
          },
          function (err) {
            fastify.log.error('TV initialization error:', err)
          }
        )
      })
    }
  })

  // Функция запуска рекламы
  const showAd = async (deviceId) => {
    const tvId = `tv${deviceId}`
    const pathToAD = `${process.env.APP_IP}:${process.env.FASTIFY_PORT}/public/${process.env.AD_FILE_NAME}`

    global[tvId].request('ssap://com.webos.applicationManager/launch', {
      id: 'com.webos.app.photovideo',
      params: { payload: [{
        fullPath: pathToAD,
        artist: '',
        subtitle: '',
        dlnaInfo: {
          flagVal: 4096,
          cleartextSize: '-1',
          contentLength: '-1',
          opVal: 1,
          protocolInfo: 'http-get:*:video/x-matroska:DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01700000000000000000000000000000',
          duration: 0
        },
        mediaType: 'VIDEO',
        thumbnail: '',
        deviceType: 'DMR',
        album: '',
        fileName: 'AD',
        lastPlayPosition: -1
      }]}
    }, function (err, res) {
      if (err) {
        fastify.log.error('AD video playback error:', err)
      } else {
        fastify.log.info('AD video playback successfully')
      }
    })
  }

  //* Функция удаления QR кода
  const hideQR = async (sessionId, deviceId) => {
    const tvId = `tv${deviceId}`
    const qrPathToFile = `${global.appRoot}/routes/qr/${sessionId}.png`
    const Session = fastify.db.models.Session
    const session = await Session.findOne({ where: { sessionId: sessionId } })

    // Закрытие окна с отображением QR кода
    global[tvId].request('ssap://system.launcher/close', {id: 'com.webos.app.tvsimpleviewer'}, function (err, res) {
      if (err) {
        console.log(err)
      }
      console.log(res)
    })

    // Удаление файла с QR кодом
    unlink(qrPathToFile)

    // Изменение значения режима (mode) указанной сессии на 2 (QR код не активировали)
    try {
      await session.set({
        mode: 2
      })
      await session.save()
    } catch (error) {
      console.log(`7777777session:${sessionId} on device=${deviceId} closed error`)
    }
  
    console.log(`66666session:${sessionId} on device=${deviceId} closed`)
  }

  // Функция генерации QR кода
  const generateQR = async (sessionId) => {
    const qrPathToFile = `${global.appRoot}/routes/public/${sessionId}.png`
    const qrText = `${process.env.QR_LINK}${sessionId}`

    return new Promise(function(resolve, reject) {
      QRCode.toFile(qrPathToFile, qrText, { width: 1000 }, function (err) {
        if (err) reject(err)
        resolve()
      })
    })
  }

  // Функция отображения QR кода на ТВ
  const showQR = async (deviceId, sessionId) => {
    const tvId = `tv${deviceId}`
    const qrLink = `${process.env.APP_IP}:${process.env.FASTIFY_PORT}/public/${sessionId}.png`

    return new Promise(function(resolve, reject) {
      global[tvId].request('ssap://media.viewer/open', {
        target: qrLink,
        title: 'QR код',
        description: 'Просканируйте этот код в вашем приложении',
        mimeType: 'image/png',
        iconSrc: '',
        loop: 0
      }, function (err, res) {
        if (err) {
          fastify.log.error('QR code show error:', err)
          reject(err)
        }
        resolve(res)
      })
    })
  }

  //* Вывод на ТВ следующего в очереди QR кода
  const showNextQR = async (deviceId) => {
    const Session = fastify.db.models.Session
    const session = await Session.findOne({
      where: {
        deviceId: deviceId,
        mode: 0
      },
      order: [ 'id' ]
    })

    if (session === null) throw `44444Sessions at deviceId=${deviceId} not found`

    // Генерация QR кода
    try {
      await generateQR(session.dataValues.sessionId)
    } catch (error) {
      throw `33333При генерации QR кода с указанным sessionId=${session.dataValues.sessionId} произошла ошибка: ${error}`
    }

    // Отображение QR кода
    try {
      await showQR(session.dataValues.deviceId, session.dataValues.sessionId)
    } catch (error) {
      throw `222222При отображении QR кода с указанным deviceId=${session.dataValues.deviceId} произошла ошибка: ${error}`
    }

    // Изменение значения режима (mode) указанной сессии на 1 (QR код отображается в данный момент)
    try {
      await session.set({
        mode: 1
      })
      await session.save()
    } catch (error) {
      console.log(`11111Ошибка изменения режима (mode) для sessionId:${session.dataValues.sessionId}`)
    }

    // Создание таймера ожидания начала сессии
    const tvTimer = `tvTimer${session.dataValues.sessionId}`
    const timeOut = process.env.QR_WAITING_TIME * 1000
    global[tvTimer] = setTimeout(hideQR, timeOut, session.dataValues.sessionId, session.dataValues.deviceId)
  }

  //* Функция проверки очереди устройства
  fastify.decorate('tvCheckQueue', async function (deviceId) {
    const Session = fastify.db.models.Session
    const sessions = await Session.findAll({ where: { deviceId: deviceId, mode: 0 }, limit: 2 })
  
    if (sessions.length === 1) {
      try {
        await showNextQR(deviceId)
      } catch (error) {
        throw error
      }
    }
  })

  //* Функция переключения ТВ на HDMI порт
  const switchPort = async (tvId, portHDMI) => {
    return new Promise(function(resolve, reject) {

      console.log(11111111111)
      //const portId = `com.webos.app.${portHDMI}`
      const portId = `com.webos.app.hdmi2`
      console.log(portId)
      console.log(tvId)
      global[tvId].request('ssap://system.launcher/launch', { id: portId },
      function (err, res) {
        if (err) {
          const error = `Ошибка переключения порта HDMI=${portHDMI} на tvId=[${tvId}: ${err}`
          console.log(22222222)
          fastify.log.error(error)
          reject(error)
        }
        console.log(333333333)
        console.log(res)
        resolve(res)
      })
    })
  }

  //* Функция завершения игровой сессии
  const hideGame = async (sessionId, deviceId) => {
    fastify.log.info(`Function(hideGame): The session:${sessionId} ending on the device:${deviceId}.`)

    const Session = fastify.db.models.Session
    const session = await Session.findOne({ where: { sessionId: sessionId } })

    // Изменение значения режима (mode) указанной сессии на 5 (сессия завершена по истечению таймера)
    try {
      await session.set({
        mode: 5
      })
      await session.save()
    } catch (err) {
      const error = `Error saving changes to the database: ${err}`
      fastify.log.error(error)
    }
  
    fastify.log.info(`Function(hideGame): The session:${sessionId} running on the device:${deviceId} ended successfully.`)
  }

  //* Функция запуска игры на ТВ
  fastify.decorate('tvStartGame', async function (sessionId) {
    const Session = fastify.db.models.Session
    const session = await Session.findOne({ where: { sessionId: sessionId, mode: 1 } })
  
    if (session === null) {
      throw `Указанная сессия с указанным sessionId=${sessionId} не найдена`
    }

    // Переключение на указанный в справочнике устройств HDMI порт
    const Device = fastify.db.models.Device
    const device = await Device.findByPk(session.dataValues.deviceId)

    if (device === null) {
      throw `Указанное устройство с указанным deviceId=${session.dataValues.deviceId} не найдено`
    }

    if (device.dataValues.portPS === null || device.dataValues.portPS === '') {
      throw `В указанном устройстве с указанным deviceId=${session.dataValues.deviceId} не указан порт подключения`
    }

    const tvId = `tv${session.dataValues.deviceId}`
    try {
      await switchPort(tvId, device.dataValues.portPS)
    } catch (error) {
      throw error
    }

    // Установка режима (mode) сессии в 3 (сессия активна на данный момент)
    try {
      await session.set({
        mode: 3,
        startTime: fastify.db.fn('NOW')
      })
      await session.save()
    } catch (error) {
      throw `Ошибка сохранения изменений в базе данных: ${error}`
    }

    // Создание таймера ожидания окончания времени игры
    const tvTimerSwitchHDMI = `globalTVTimerSwitchHDMI${session.dataValues.sessionId}`
    const timeOut = session.dataValues.time * 1000

    global[tvTimerSwitchHDMI] = setTimeout(hideGame, timeOut, session.dataValues.sessionId, session.dataValues.deviceId)
  })
})