'use strict'

import dgram from 'dgram'

const ssdpMsg = Buffer.from('M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: "ssdp:discover"\r\nMX: 5\r\nST: urn:dial-multiscreen-org:service:dial:1\r\nUSER-AGENT: iOS/5.0 UDAP/2.0 iPhone/4\r\n\r\n')
const ssdpPort = 1900
const ssdpAddress = '239.255.255.250'

export const discover_ip = async (timeout) => {
  const server = dgram.createSocket('udp4')
  const ipArray = {
    error: false,
    addresses: []
  }

  server.on('error', (err) => {
    console.error(`server error:\n${err.stack}`)
    ipArray.error = true
    server.close()
  })

  server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
    server.send(ssdpMsg, 0, ssdpMsg.length, ssdpPort, ssdpAddress, (err) => {
      if (err) {
        console.error(`server listening error:\n${err}`)
        ipArray.error = true
        throw err
      }
    })
  })

  server.on('message', (msg, rinfo) => {
    console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`)
    if (msg.indexOf('WebOS')) {
      if (ipArray.addresses.indexOf(rinfo.address) < 0) {
        ipArray.addresses.push(rinfo.address)
      }
    }
  })

  server.bind()

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      server.close()
      resolve(ipArray)
    }, timeout * 1000)
  })
}