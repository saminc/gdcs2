'use strict'

export async function modelBranchInit(sequelize, DataTypes) {
  return sequelize.define('Branch', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    cityId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: sequelize.models.City,
        key: 'id'
      }
    },
    address: {
      type: DataTypes.STRING,
    },
    openingTimes: {
      type: DataTypes.STRING,
    },
    lat: {
      type: DataTypes.STRING,
    },
    lng: {
      type: DataTypes.STRING,
    },    
    description: {
      type: DataTypes.STRING,
    }
  }, {
    tableName: 'branches',
    timestamps: false,
    indexes: [
      {
        fields: [ 'cityId', 'name' ],
        unique: true
      }
    ]
  })
}