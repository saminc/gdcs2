'use strict'

export async function modelCityInit(sequelize, DataTypes) {
  return sequelize.define('City', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      unique: true
    },
    name: {
      type: DataTypes.STRING(32),
      allowNull: false,
      unique: true
    },    
    description: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'cities',
    timestamps: false,
    indexes: [
      {
        fields: ['name'],
        unique: true
      }
    ]
  })
}