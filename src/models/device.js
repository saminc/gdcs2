'use strict'

export async function modelDeviceInit(sequelize, DataTypes) {
  return sequelize.define('Device', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    branchId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: sequelize.models.Branch,
        key: 'id'
      }
    },
    ipTV: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: true
    },
    ipPS: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: true
    },
    portPS: {
      type: DataTypes.STRING(5),
      allowNull: false,
      unique: true
    },
    disable: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    description: {
      type: DataTypes.STRING
    }
  }, {
    tableName: 'devices',
    timestamps: false,
    indexes: [
      {
        fields: [ 'branchId', 'name' ],
        unique: true
      }
    ]
  })
}