'use strict'

export async function modelSessionInit(sequelize, DataTypes, Sequelize) {
  return sequelize.define('Session', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    sessionId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true
    },
    deviceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: sequelize.models.Device,
        key: 'id'
      }
    },
    time: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    creationTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('NOW')
    },
    startTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    mode: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
      // 0 - сессия создана
      // 1 - QR код сессии отправлен на ТВ
      // 2 - QR код сессии не считан за определенное время и данная сессия пропускается
      // 3 - сессия активна на данный момент
      // 4 - сессия завершена досрочно
      // 5 - сессия завершена по истечению таймера
    }
  }, {
    tableName: 'sessions',
    timestamps: false,
    indexes: [
      {
        fields: [ 'sessionId', 'deviceId', 'mode' ]
      }
    ]
  })
}