//* Модуль операций со всеми записями активных сессий
'use strict'

import {
  schemaSessionsGet,
  schemaSessionsDelete
} from './schemas.js'

//* Функция возвращает все записи активных сессий
const sessionGet = async (fastify, reply) => {
  // const Branch = fastify.db.models.Branch
  // const City = fastify.db.models.City
  // const Device = fastify.db.models.Device

  // try {
  //   const devices = await Device.findAll({
  //     include: [{
  //       model: Branch,
  //       include: [{ model: City, attributes: [] }],
  //       attributes: []
  //     }],
  //     attributes: [
  //       'id',
  //       'name',
  //       [fastify.db.literal('"Branch"."id"'), 'branchId'],
  //       [fastify.db.literal('"Branch"."name"'), 'branchName'],
  //       [fastify.db.literal('"Branch->City"."name"'), 'city'],
  //       [fastify.db.literal('"Branch"."address"'), 'address'],
  //       [fastify.db.literal('"Branch"."openingTimes"'), 'openingTimes'],
  //       [fastify.db.literal('"Branch"."lat"'), 'lat'],
  //       [fastify.db.literal('"Branch"."lng"'), 'lng'],
  //       'ipTV',
  //       'ipPS',
  //       'disable',
  //       'description'
  //     ]
  //   })

  //   return JSON.stringify(devices, null, 2)
  // } catch (error) {
  //   return reply.status(400).send({ message: error })
  // }
  return []
}

const sessionsDelete = async (reply) => {

  return
}

export default async function (fastify, opts) {
  fastify.get('/', schemaSessionsGet, async (reply) => sessionGet(fastify, reply))
  fastify.delete('/', schemaSessionsDelete, async (reply) => sessionsDelete(reply))
}