'use strict'

export const schemaSessionsGet = {
  schema: {
    tags: [ 'sessions' ],
    description: 'Возвращает список всех активных сессий',
    response: {
      200: {
        type: 'array',
        description: 'Информация о сессиях успешно передана',
        items: {
          type: 'object',
          minProperties: 14,
          maxProperties: 14,
          properties: {
            sessionId: { $ref: 'schemaSessionId#' },
            sessionStart: { type: 'string', description: 'Дата и время начала сессии' },
          }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaSessionsDelete = {
  schema: {
    tags: [ 'sessions' ],
    description: 'Удаляет все игровые сессии и выходит из активных если они есть',
    response: {
      200: {
        type: 'object',
        description: 'Все сессии удалены успешно',
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}