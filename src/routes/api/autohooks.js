'use strict'

export default async function (fastify, opts) {
  fastify.addHook('onRequest', async (request, reply) => {
    try {
      const apiKey = request.headers['x-api-key'] || ''
      if (apiKey !== process.env.SECURITY_API_KEY) {
        reply.code(401).send('Unauthorized')
      }
    } catch (err) {
      reply.send(err)
    }
  })
}