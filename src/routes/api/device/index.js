//* Модуль проверки и возврата количества человек в очереди по указанному deviceId
'use strict'

import {
  schemaDeviceGet,
  schemaDeviceHealth,
  schemaDeviceQueueInfo
} from './schemas.js'

//* Функция возвращает данные записи Id устройства из справочника устройств
const deviceGet = async (fastify, request, reply) => {
  const { id } = request.params

  const Branch = fastify.db.models.Branch
  const City = fastify.db.models.City
  const Device = fastify.db.models.Device

  try {
    const device = await Device.findByPk(id, {
      include: [{
        model: Branch,
        include: [{ model: City, attributes: [] }],
        attributes: []
      }],
      attributes: [
        'id',
        'name',
        [fastify.db.literal('"Branch"."id"'), 'branchId'],
        [fastify.db.literal('"Branch"."name"'), 'branchName'],
        [fastify.db.literal('"Branch->City"."id"'), 'cityId'],
        [fastify.db.literal('"Branch->City"."name"'), 'city'],
        [fastify.db.literal('"Branch"."address"'), 'address'],
        [fastify.db.literal('"Branch"."openingTimes"'), 'openingTimes'],
        [fastify.db.literal('"Branch"."lat"'), 'lat'],
        [fastify.db.literal('"Branch"."lng"'), 'lng'],
        'ipTV',
        'ipPS',
        'disable',
        'description'
      ]
    })

    if (device === null) {
      return reply.status(400).send({ message: 'Указанный id устройства не найден' })
    } else {
      return reply.send(device.toJSON())
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция возвращает доступность устройства
const getDeviceHealth = async (fastify, request, reply) => {
  const { id } = request.params
  const Device = fastify.db.models.Device

  try {
    const device = await Device.findByPk(id)

    if (device === null) {
      return reply.status(400).send({ message: 'Указанный id устройства не найден' })
    }

    return reply.send({
      'errorCode': 7,
      'errorMessage': `Подключение к устройству deviceId: |${id}| отсутствует`,
      'resultData': null
    })
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

const getDeviceQueue = async (fastify, request, reply) => {
  const { id } = request.params
  const Device = fastify.db.models.Device

  try {
    const device = await Device.findByPk(id)

    if (device === null) {
      return reply.status(400).send({ message: 'Указанный id устройства не найден' })
    }

    const result = {
      queueCount: 0,
      maxTime: 0
    }

    return reply.send(result)
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  fastify.get('/:id', schemaDeviceGet, async (request, reply) => deviceGet(fastify, request, reply))
  fastify.get('/:id/health', schemaDeviceHealth, async (request, reply) => getDeviceHealth(fastify, request, reply))
  fastify.get('/:id/queue', schemaDeviceQueueInfo, async (request, reply) => getDeviceQueue(fastify, request, reply))
}