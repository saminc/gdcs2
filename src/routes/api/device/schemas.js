'use strict'

export const schemaDeviceGet = {
  schema: {
    tags: [ 'device' ],
    description: 'Возвращает запись из справочника об указанном устройстве',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647,
          description: 'Id устройства'
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись об устройстве передана успешно',
        minProperties: 13,
        maxProperties: 13,
        properties: {
          id: { $ref: 'schemaDeviceId#' },
          name: { type: 'string', description: 'Условное наименование игрового устройства' },
          branchId: { type: 'string', description: 'ID отделения банка' },
          branchName: { type: 'string', description: 'Наименование отделения банка' },
          cityId: {
            type: 'integer',
            format: 'int32',
            minimum: 1,
            maximum: 2147483647,
            description: 'Код города расположения отделения банка / игрового устройства'
          },
          city: { type: 'string', description: 'Город расположения отделения банка / игрового устройства' },
          address: { type: 'string', description: 'Адрес отделения банка / игрового устройства' },
          openingTimes: { type: 'string', description: 'Режим работы отделения банка / игрового устройства' },
          lat: { type: 'string', description: 'Географическая широта месторасположения отделения банка / игрового устройства' },
          lng: { type: 'string', description: 'Географическая долгота месторасположения отделения банка / игрового устройства' },
          ipTV: { type: 'string', description: 'IP адрес телевизора подключенного к игровому устройству' },
          ipPS: { type: 'string', description: 'Ip адрес игрового устройства' },
          disable: { type: 'boolean', description: 'Признак отключенного устройства' },
          deviceDescription: { type: 'string', description: 'Примечание к игровому устройству' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaDeviceHealth = {
  schema: {
    tags: [ 'device' ],
    description: 'Возвращает состояние указанного устройства',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: { $ref: 'schemaDeviceId#' }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись о состоянии устройства передана успешно',
        minProperties: 3,
        maxProperties: 3,
        properties: {
          errorCode: {
            type: 'integer',
            format: 'int32',
            minimum: 1,
            maximum: 2147483647,
            description: 'Id ошибки или 0 если она отсутствует'
          },
          errorMessage: { type: 'string', description: 'Сообщение об ошибке или "" если она отсутствует' },
          resultData : { type: 'string', description: 'Возвращает "Ок" если без ошибок и null если устройство не доступно' },
        }
      },
      400: {
        type: 'object',
        description: 'Переданный id не соответствует формату или устройство с указанным id не найдено',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaDeviceQueueInfo = {
  schema: {
    tags: [ 'device' ],
    description: 'Возвращает количество человек в очереди по указанному Id и максимальное время ожидания',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        deviceId: { $ref: 'schemaDeviceId#' }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запрос завершился успешно',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          queueCount: { $ref: 'schemaDeviceQueue#' },
          maxTime: {
            type: 'integer',
            format: 'int32',
            minimum: 1,
            maximum: 2147483647,
            description: 'Максимальное время ожидания в секундах'
          }
        }
      },
      400: {
        type: 'object',
        description: 'Переданный id не соответствует формату или устройство с указанным id не найдено',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}