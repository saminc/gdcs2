'use strict'

export const schemaTVsGet = {
  schema: {
    description: 'Возвращает информацию о найденных телевизорах',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'timeout' ],
      properties: {
        timeout: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 60
        }
      }
    },
    response: {
      200: {
        type: 'array',
        description: 'Записи о телевизорах переданы успешно'
      },
      400: {
        type: 'object',
        description: 'Сообщение об ошибке',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}