//* Модуль операций со всеми телевизорами
'use strict'

import { schemaTVsGet } from './schemas.js'
import { discover_ip } from '../../../../../libs/lgtv3/index.js'

//* Функция возвращает все найденные телевизоры
const tvsGet = async (request, reply) => {
  const { timeout } = request.params

  try {
    const tvArray = await discover_ip(timeout)

    if (tvArray.error) {
      return reply.code(400).send({ message: 'Failed to find TV`s IP address on the LAN. Verify that TV is on, and that you are on the same LAN/Wifi.' })
    }

    console.log(tvArray)

    return tvArray.addresses
  } catch (error) {
    return reply.code(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  fastify.get('/:timeout', schemaTVsGet, tvsGet)
}