'use strict'

export const schemaDevicesGet = {
  schema: {
    description: 'Возвращает записи из справочника обо всех устройствах',
    response: {
      200: {
        type: 'array',
        description: 'Записи об устройствах переданы успешно',
        items: {
          type: 'object',
          minProperties: 7,
          maxProperties: 7,
          properties: {
            id: { type: 'integer' },
            name: { type: 'string' },
            branchId: { type: 'integer' },
            ipTV: { type: 'string' },
            ipPS: { type: 'string' },
            disable: { type: 'boolean' },
            description: { type: 'string' }
          }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}