//* Модуль операций со справочником отделений банка
'use strict'

import {
  schemaBranchPost,
  schemaBranchPut,
  schemaBranchGet,
  schemaBranchDelete
} from './schemas.js'

//* Функция добавляет запись в справочник отделений банка
const branchNew = async (Branch, request, reply) => {
  const { cityId, address, name, openingTimes, lat, lng, description } = request.body

  try {
    const branch = await Branch.findOne({ where: { name: name } })

    if (branch === null) {
      await Branch.create({
        cityId: cityId,
        address: address,
        name: name,
        openingTimes: openingTimes,
        lat: lat,
        lng: lng,
        description: description
      })
  
      return reply.status(201).send()
    } else {

      return reply.status(400).send({ message: `Указанное имя отделения "${name}" уже существует` })
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция изменяет данные записи в справочнике отделений банка
const branchPut = async (Branch, request, reply) => {
  const { branchId } = request.params
  const { cityId, address, name, openingTimes, lat, lng, description } = request.body

  try {
    const branch = await Branch.findByPk(branchId)

    if (branch === null) {
      return reply.status(400).send({ message: 'Указанный branchId не найден' })
    } else {
      branch.set({
        cityId: cityId,
        address: address,
        name: name,
        openingTimes: openingTimes,
        lat: lat,
        lng: lng,
        description: description
      })
      await branch.save()

      return reply.send()
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция возвращает данные записи из справочника отделений банка
const branchGet = async (Branch, request, reply) => {
  const { branchId } = request.params

  try {
    const branch = await Branch.findByPk(branchId)

    if (branch === null) {
      return reply.status(400).send({ message: 'Указанный branchId не найден' })
    } else {
      return reply.status(200).send(branch.toJSON())
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция удаляет данные записи из справочника отделений банка
const branchDelete = async (Branch, request, reply) => {
  const { branchId } = request.params

  try {
    const branch = await Branch.findByPk(branchId)

    if (branch === null) {
      return reply.status(400).send({ message: 'Указанный branchId не найден' })
    } else {
      await branch.destroy()
      return reply.status(200).send()
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  const Branch = fastify.db.models.Branch

  fastify.post('/', schemaBranchPost, async (request, reply) => branchNew(Branch, request, reply))
  fastify.put('/:branchId', schemaBranchPut, async (request, reply) => branchPut(Branch, request, reply))
  fastify.get('/:branchId', schemaBranchGet, async (request, reply) => branchGet(Branch, request, reply))
  fastify.delete('/:branchId', schemaBranchDelete, async (request, reply) => branchDelete(Branch, request, reply))
}