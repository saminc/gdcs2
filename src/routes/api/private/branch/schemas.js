'use strict'

export const schemaBranchPost = {
  schema: {
    description: 'Добавляет запись в справочник о новом отделении банка',
    body: {
      type: 'object',
      required: ['cityId', 'name'],
      minProperties: 2,
      maxProperties: 7,
      additionalProperties: false,
      properties: {
        cityId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        },
        address: {
          type: 'string',
          minLength: 2,
          maxLength: 255
        },
        name: {
          type: 'string',
          minLength: 2,
          maxLength: 255
        },
        openingTimes: {
          type: 'string',
          minLength: 3,
          maxLength: 11
        },
        lat: {
          type: 'string',
          minLength: 1,
          maxLength: 16
        },
        lng: {
          type: 'string',
          minLength: 1,
          maxLength: 16
        },
        description: {
          type: 'string',
          maxLength: 255
        }
      }
    },
    response: {
      201: {
        type: 'object',
        description: 'Запись о новом отделении банка создана успешно',
      },
      400: {
        type: 'object',
        description: 'Переданные данные не соответствует формату или отделение банка с таким именем уже ранее были создано',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaBranchPut = {
  schema: {
    description: 'Вносит изменения в запись справочника об отделении банка',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'branchId' ],
      properties: {
        branchId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    body: {
      type: 'object',
      minProperties: 1,
      maxProperties: 7,
      additionalProperties: false,
      properties: {
        cityId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        },
        address: {
          type: 'string',
          minLength: 2,
          maxLength: 255
        },
        name: {
          type: 'string',
          minLength: 2,
          maxLength: 255
        },
        openingTimes: {
          type: 'string',
          minLength: 3,
          maxLength: 11
        },
        lat: {
          type: 'string',
          minLength: 1,
          maxLength: 16
        },
        lng: {
          type: 'string',
          minLength: 1,
          maxLength: 16
        },
        description: {
          type: 'string',
          maxLength: 255
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Изменения записи об отделении банка произведены успешно',
      },
      400: {
        type: 'object',
        description: 'Переданные данные не соответствует формату или branchId не найден',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaBranchGet = {
  schema: {
    description: 'Возвращает запись из справочника об указанном отделении банка',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'branchId' ],
      properties: {
        branchId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись об отделении банка передана успешно',
        minProperties: 8,
        maxProperties: 8,
        properties: {
          id: { type: 'integer' },
          cityId: { type: 'string' },
          address: { type: 'string' },
          name: { type: 'string' },
          openingTimes: { type: 'string' },
          lat: { type: 'string' },
          lng: { type: 'string' },
          description: { type: 'string' }
        }
      },
      400: {
        type: 'object',
        description: 'Переданный в параметрах branchId не соответствует формату',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaBranchDelete = {
  schema: {
    description: 'Удаляет запись из справочника об указанном отделении банка',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'branchId' ],
      properties: {
        branchId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись об отделении банка удалена успешно',
      },
      400: {
        type: 'object',
        description: 'Переданный в параметрах branchId не соответствует формату',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}