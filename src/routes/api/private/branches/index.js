//* Модуль операций со всеми записями справочника отделений банка
'use strict'

import { schemaBranchesGet } from './schemas.js'

//* Функция возвращает все записи из справочника отделений банка
const branchesGet = async (fastify, reply) => {
  const Branch = fastify.db.models.Branch
  const City = fastify.db.models.City

  try {
    const branches = await Branch.findAll({
      include: [{ model: City, attributes: [] }],
      attributes: [
        'id',
        'name',
        [fastify.db.literal('"City"."name"'), 'city'],
        'address',
        'openingTimes',
        'lat',
        'lng',
        'description'
      ]
    })

    return JSON.stringify(branches, null, 2)
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  fastify.get('/', schemaBranchesGet, async (reply) => branchesGet(fastify, reply))
}