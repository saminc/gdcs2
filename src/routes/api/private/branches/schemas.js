'use strict'

export const schemaBranchesGet = {
  schema: {
    description: 'Возвращает записи из справочника обо всех отделений банка',
    response: {
      200: {
        type: 'array',
        description: 'Записи об отделениях банка переданы успешно',
        items: {
          type: 'object',
          minProperties: 8,
          maxProperties: 8,
          properties: {
            id: { type: 'integer' },
            name: { type: 'string' },
            city: { type: 'string' },
            address: { type: 'string' },
            openingTimes: { type: 'string' },
            lat: { type: 'string' },
            lng: { type: 'string' },
            description: { type: 'string' }
          }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}