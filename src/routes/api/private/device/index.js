//* Модуль операций со справочником устройств
'use strict'

import {
  schemaDevicePost,
  schemaDevicePut,
  schemaDeviceGet,
  schemaDeviceDelete
} from './schemas.js'

//* Функция добавляет запись в справочник устройств
const deviceNew = async (Device, request, reply) => {
  const { name, branchId, ipTV, ipPS, portPS, disable, description } = request.body

  try {
    const device = await Device.findOne({ where: { name: name } })

    if (device === null) {
      await Device.create({
        name: name,
        branchId: branchId,
        ipTV: ipTV,
        ipPS: ipPS,
        portPS: portPS,
        disable: disable,
        description: description
      })

      return reply.status(201).send()
    } else {
      return reply.status(400).send({ message: `Указанное имя устройства "${name}" уже существует` })
    }
  } catch (error) {
    console.log(error)
    return reply.status(400).send({ message: error })
  }
}

//* Функция изменяет данные записи в справочнике устройств
const devicePut = async (Device, request, reply) => {
  const { id } = request.params
  const { name, branchId, ipTV, ipPS, portPS, disable, description } = request.body

  try {
    const device = await Device.findByPk(id)

    if (device === null) {
      return reply.status(400).send({ message: 'Указанный id не найден' })
    } else {
      device.set({
        name: name,
        branchId: branchId,
        ipTV: ipTV,
        ipPS: ipPS,
        portPS: portPS,
        disable: disable,
        description: description
      })
      await device.save()

      return reply.send()
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция возвращает данные записи из справочника устройств
const deviceGet = async (Device, request, reply) => {
  const { id } = request.params

  try {
    const device = await Device.findByPk(id)

    if (device === null) {
      return reply.status(400).send({ message: 'Указанный id не найден' })
    } else {
      return reply.send(device.toJSON())
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция удаляет данные записи из справочника устройств
const deviceDelete = async (Device, request, reply) => {
  const { id } = request.params

  try {
    const device = await Device.findByPk(id)

    if (device === null) {
      return reply.status(400).send({ message: 'Указанный id не найден' })
    } else {
      await device.destroy()
      return reply.send()
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  const Device = fastify.db.models.Device

  fastify.post('/', schemaDevicePost, async (request, reply) => deviceNew(Device, request, reply))
  fastify.put('/:id', schemaDevicePut, async (request, reply) => devicePut(Device, request, reply))
  fastify.get('/:id', schemaDeviceGet, async (request, reply) => deviceGet(Device, request, reply))
  fastify.delete('/:id', schemaDeviceDelete, async (request, reply) => deviceDelete(Device, request, reply))
}