'use strict'

export const schemaDevicePost = {
  schema: {
    description: 'Добавляет запись в справочник о новом устройстве',
    body: {
      type: 'object',
      required: [ 'name', 'branchId', 'ipTV', 'ipPS', 'portPS' ],
      minProperties: 5,
      maxProperties: 7,
      additionalProperties: false,
      properties: {
        name: {
          type: 'string',
          minLength: 1,
          maxLength: 255
        },
        branchId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        },
        ipTV: {
          type: 'string',
          minLength: 7,
          maxLength: 15
        },
        ipPS: {
          type: 'string',
          minLength: 7,
          maxLength: 15
        },
        portPS: {
          type: 'string',
          minLength: 5,
          maxLength: 5
        },
        disable:  {
          type: 'boolean'
        },
        description: {
          type: 'string',
          maxLength: 255
        }
      }
    },
    response: {
      201: {
        type: 'object',
        description: 'Запись о новом устройстве создана успешно',
      },
      400: {
        type: 'object',
        description: 'Переданные данные не соответствует формату или устройство с именем name уже ранее были создано',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaDevicePut = {
  schema: {
    description: 'Вносит изменения в запись справочника об устройстве',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    body: {
      type: 'object',
      minProperties: 6,
      maxProperties: 6,
      additionalProperties: false,
      properties: {
        name: {
          type: 'string',
          minLength: 1,
          maxLength: 255
        },
        branchId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        },
        ipTV: {
          type: 'string',
          minLength: 7,
          maxLength: 15
        },
        ipPS: {
          type: 'string',
          minLength: 7,
          maxLength: 15
        },
        disable:  {
          type: 'boolean'
        },
        description: {
          type: 'string',
          maxLength: 255
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Изменения записи об устройстве произведены успешно',
      },
      400: {
        type: 'object',
        description: 'Переданные данные не соответствует формату или deviceId не найден',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaDeviceGet = {
  schema: {
    description: 'Возвращает запись из справочника об указанном устройстве',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись об устройстве передана успешно',
        minProperties: 7,
        maxProperties: 7,
        properties: {
          id: { type: 'integer' },
          name: { type: 'string' },
          branchId: { type: 'integer' },
          ipTV: { type: 'string' },
          ipPS: { type: 'string' },
          disable:  { type: 'boolean' },
          description: { type: 'string' }
        }
      },
      400: {
        type: 'object',
        description: 'Переданный в параметрах deviceId данные не соответствует формату',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaDeviceDelete = {
  schema: {
    description: 'Удаляет запись из справочника об указанном устройстве',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись об устройстве удалена успешно',
      },
      400: {
        type: 'object',
        description: 'Переданный в параметрах deviceId данные не соответствует формату',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}