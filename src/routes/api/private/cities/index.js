//* Модуль операций со всеми записями справочника отделений банка
'use strict'

import { schemaCitiesGet } from './schemas.js'

//* Функция возвращает все записи из справочника городов
const citesGet = async (City, reply) => {
  try {
    const cites = await City.findAll()

    return JSON.stringify(cites, null, 2)
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  const City = fastify.db.models.City

  fastify.get('/', schemaCitiesGet, async (reply) => citesGet(City, reply))
}