'use strict'

export const schemaCitiesGet = {
  schema: {
    description: 'Возвращает записи из справочника обо всех городах',
    response: {
      200: {
        type: 'array',
        description: 'Записи о городах переданы успешно',
        items: {
          type: 'object',
          minProperties: 3,
          maxProperties: 3,
          properties: {
            id: { type: 'integer' },
            name: { type: 'string' },
            description: { type: 'string' }
          }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}