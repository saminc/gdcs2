'use strict'

export const schemaCityPost = {
  schema: {
    description: 'Добавляет запись в справочник о новом городе',
    body: {
      type: 'object',
      required: [ 'id', 'name' ],
      minProperties: 2,
      maxProperties: 3,
      additionalProperties: false,
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        },
        name: {
          type: 'string',
          minLength: 2,
          maxLength: 255
        },
        description: {
          type: 'string',
          maxLength: 255
        }
      }
    },
    response: {
      201: {
        type: 'object',
        description: 'Запись о новом городе создана успешно',
      },
      400: {
        type: 'object',
        description: 'Переданные данные не соответствует формату или город с таким именем или id уже ранее был создан',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaCityPut = {
  schema: {
    description: 'Вносит изменения в запись справочника городов',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    body: {
      type: 'object',
      minProperties: 2,
      maxProperties: 2,
      additionalProperties: false,
      properties: {
        name: {
          type: 'string',
          minLength: 2,
          maxLength: 255
        },
        description: {
          type: 'string',
          maxLength: 255
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Изменения записи о городе произведены успешно',
      },
      400: {
        type: 'object',
        description: 'Переданные данные не соответствует формату или id не найден',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaCityGet = {
  schema: {
    description: 'Возвращает запись из справочника об указанном городе',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        id: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись о городе передана успешно',
        minProperties: 3,
        maxProperties: 3,
        properties: {
          id: { type: 'integer' },
          name: { type: 'string' },
          description: { type: 'string' }
        }
      },
      400: {
        type: 'object',
        description: 'Переданный в параметрах id не соответствует формату',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaCityDelete = {
  schema: {
    description: 'Удаляет запись из справочника о указанном городе',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'id' ],
      properties: {
        cityId: {
          type: 'integer',
          format: 'int32',
          minimum: 1,
          maximum: 2147483647
        }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Запись о городе удалена успешно',
      },
      400: {
        type: 'object',
        description: 'Переданный в параметрах id не соответствует формату',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}