//* Модуль операций со справочником городов
'use strict'

import {
  schemaCityPost,
  schemaCityPut,
  schemaCityGet,
  schemaCityDelete
} from './schemas.js'

//* Функция добавляет запись в справочник городов
const cityNew = async (City, request, reply) => {
  const { id, name, description } = request.body

  let city = await City.findByPk(id)

  if (city !== null) {
    return reply.status(400).send({ message: 'Указанный id уже существует' })
  }

  city = await City.findOne({ where: { name: name } })

  if (city !== null) {
    return reply.status(400).send({ message: `Указанное наименование города "${name}" уже существует` })
  }

  try {
    await City.create({
      id: id,
      name: name,
      description: description
    })
  
    return reply.status(201).send()
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

//* Функция изменяет данные записи в справочнике городов
const cityPut = async (City, request, reply) => {
  const { id } = request.params
  const { name, description } = request.body

  const city = await City.findByPk(id)

  if (city === null) {
    return reply.status(400).send({ message: 'Указанный id не найден' })
  } else {
    try {
      city.set({
        id: id,
        name: name,
        description: description
      })
      await city.save()

      return reply.send()
    } catch (error) {
      return reply.status(400).send({ message: error })
    }
  }
}

//* Функция возвращает данные записи из справочника городов
const cityGet = async (City, request, reply) => {
  const { id } = request.params

  const city = await City.findByPk(id)

  if (city === null) {
    return reply.status(400).send({ message: 'Указанный id не найден' })
  } else {
    return reply.status(200).send(city.toJSON())
  }
}

//* Функция удаляет данные записи из справочника городов
const cityDelete = async (City, request, reply) => {
  const { id } = request.params

  try {
    const city = await City.findByPk(id)

    if (city === null) {
      return reply.status(400).send({ message: 'Указанный id не найден' })
    } else {
      await city.destroy()
      return reply.status(200).send()
    }
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  const City = fastify.db.models.City

  fastify.post('/', schemaCityPost, async (request, reply) => cityNew(City, request, reply))
  fastify.put('/:id', schemaCityPut, async (request, reply) => cityPut(City, request, reply))
  fastify.get('/:id', schemaCityGet, async (request, reply) => cityGet(City, request, reply))
  fastify.delete('/:id', schemaCityDelete, async (request, reply) => cityDelete(City, request, reply))
}