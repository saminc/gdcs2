'use strict'

export const schemaDevicesGet = {
  schema: {
    tags: ['devices'],
    description: 'Возвращает список всех устройств',
    response: {
      200: {
        type: 'array',
        description: 'Информация об устройствах успешно передана',
        items: {
          type: 'object',
          minProperties: 14,
          maxProperties: 14,
          properties: {
            id: { $ref: 'schemaDeviceId#' },
            name: { type: 'string', description: 'Условное наименование игрового устройства' },
            branchId: { type: 'string', description: 'ID отделения банка' },
            branchName: { type: 'string', description: 'Наименование отделения банка' },
            city: { type: 'string', description: 'Город расположения отделения банка/игрового устройства' },
            address: { type: 'string', description: 'Адрес отделения банка/игрового устройства' },
            openingTimes: { type: 'string', description: 'Режим работы отделения банка/игрового устройства' },
            lat: { type: 'string', description: 'Географическая широта месторасположения отделения банка/игрового устройства' },
            lng: { type: 'string', description: 'Географическая долгота месторасположения отделения банка/игрового устройства' },
            ipTV: { type: 'string', description: 'IP адрес телевизора подключенного к игровому устройству' },
            ipPS: { type: 'string', description: 'Ip адрес игрового устройства' },
            disable: { type: 'boolean', description: 'Признак отключенного устройства' },
            city: { type: 'string', description: 'Город расположения отделения банка' },
            deviceDescription: { type: 'string', description: 'Примечание к игровому устройству' }
          }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}