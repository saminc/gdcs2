//* Модуль операций со всеми записями справочника устройств
'use strict'

import { schemaDevicesGet } from './schemas.js'

//* Функция возвращает все записи из справочника устройств
const devicesGet = async (fastify, reply) => {
  const Branch = fastify.db.models.Branch
  const City = fastify.db.models.City
  const Device = fastify.db.models.Device

  try {
    const devices = await Device.findAll({
      include: [{
        model: Branch,
        include: [{ model: City, attributes: [] }],
        attributes: []
      }],
      attributes: [
        'id',
        'name',
        [fastify.db.literal('"Branch"."id"'), 'branchId'],
        [fastify.db.literal('"Branch"."name"'), 'branchName'],
        [fastify.db.literal('"Branch->City"."name"'), 'city'],
        [fastify.db.literal('"Branch"."address"'), 'address'],
        [fastify.db.literal('"Branch"."openingTimes"'), 'openingTimes'],
        [fastify.db.literal('"Branch"."lat"'), 'lat'],
        [fastify.db.literal('"Branch"."lng"'), 'lng'],
        'ipTV',
        'ipPS',
        'disable',
        'description'
      ]
    })

    return JSON.stringify(devices, null, 2)
  } catch (error) {
    return reply.status(400).send({ message: error })
  }
}

export default async function (fastify, opts) {
  fastify.get('/', schemaDevicesGet, async (reply) => devicesGet(fastify, reply))
}