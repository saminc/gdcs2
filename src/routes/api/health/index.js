//* Модуль проверки состояния и времени отклика сервера
'use strict'

import { schemaHealth } from './schemas.js'

const healthCheck = async () => {
  return { status: 'ok', timestamp: new Date().toISOString() }
}

export default async function (fastify, opts) {
  fastify.get('/', schemaHealth, async () => healthCheck())
}