'use strict'

export const schemaHealth = {
  schema: {
    tags: ['health'],
    description: 'Проверка состояния сервера',
    response: {
      200: {
        type: 'object',
        minProperties: 2,
        maxProperties: 2,
        properties: {
          status: { type: 'string' },
          timestamp: { type: 'string', format: 'date-time' }
        }
      }
    },
    security: [{ apiKey: [] }]
  }
}