'use strict'
//* Модуль работы с сессиями

import { schemaSessionPost, schemaSessionPut, schemaSessionDelete } from './schemas.js'
import newSession from '../../../controllers/session/new.js'
import startSession from '../../../controllers/session/start.js'
import destroySession from '../../../controllers/session/destroy.js'

//* Функция создания новой игровой сессии
const sessionNew = async (fastify, request, reply) => {
  const { deviceId, sessionId, sessionTime } = request.body

  let
    errorCode = null,
    errorMessage = null,
    resultData = null

  const result = newSession(fastify, deviceId, sessionId, sessionTime)

  switch (result) {
    case 0:
      resultData = 'Session created successfully'
      break
    case 1:
      errorCode = 1,
      errorMessage = `Device with deviceId=${deviceId} not found`
      break
    case 2:
      errorCode = 2,
      errorMessage = `Device with deviceId=${deviceId} not available at the moment`
      break
    case 3:
      errorCode = 3,
      errorMessage = `Session with sessionId=${sessionId} has already been created before`
      break
    case 4:
      errorCode = 4,
      errorMessage = 'An error occurred while creating the session'
      break
    case 5:
      errorCode = 5,
      errorMessage = `Error output QR-code to deviceId=${deviceId}`
      break
    default:
      errorCode = 6,
      errorMessage = 'Unexpected error'
      break
  }

  return reply.send({
    'errorCode': errorCode,
    'errorMessage': errorMessage,
    'resultData': resultData
  })
}

//* Функция запуска указанной игровой сессии
const sessionStart = async (fastify, request, reply) => {
  const { sessionId } = request.params

  let
    errorCode = null,
    errorMessage = null,
    resultData = null

  const result = startSession(fastify, sessionId)

  switch (result) {
    case 0:
      resultData = 'Session started successfully'
      break
    case 1:
      errorCode = 7,
      errorMessage = `Session with sessionId=${sessionId} not found`
      break
    case 2:
      errorCode = 8,
      errorMessage = `Session with sessionId=${sessionId} is not in display mode on TV`
      break
    case 3:
      errorCode = 9,
      errorMessage = 'Error switching TV in game mode'
      break
    default:
      errorCode = 10,
      errorMessage = 'Unexpected error'
      break
  }

  return reply.send({
    'errorCode': errorCode,
    'errorMessage': errorMessage,
    'resultData': resultData
  })
}

//* Функция удаления указанной игровой сессии
const sessionDelete = async (fastify, request, reply) => {
  const { sessionId } = request.params
  
  let
    errorCode = null,
    errorMessage = null,
    resultData = null

  const result = destroySession(fastify, sessionId)

  switch (result) {
    case 0:
      resultData = 'Session deleted successfully'
      break
    case 1:
      errorCode = 11,
      errorMessage = `Session with sessionId=${sessionId} not found`
      break
    case 2:
      errorCode = 12,
      errorMessage = `Error switching TV in wait mode`
      break
    default:
      errorCode = 13,
      errorMessage = 'Unexpected error'
      break
  }

  return reply.send({
    'errorCode': errorCode,
    'errorMessage': errorMessage,
    'resultData': resultData
  })
}

export default async function (fastify, opts) {
  fastify.post('/', schemaSessionPost, async (request, reply) => sessionNew(fastify, request, reply))
  fastify.put('/:sessionId', schemaSessionPut, async (request, reply) => sessionStart(fastify, request, reply))
  fastify.delete('/:sessionId', schemaSessionDelete, async (request, reply) => sessionDelete(fastify, request, reply))
}