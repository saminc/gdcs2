'use strict'

export const schemaSessionPost = {
  schema: {
    tags: [ 'session' ],
    description: 'Создает новую игровую сессию',
    body: {
      type: 'object',
      additionalProperties: false,
      required: [ 'deviceId', 'sessionId', 'sessionTime' ],
      properties: {
        sessionId: { $ref: 'schemaSessionId#' },
        deviceId: { $ref: 'schemaDeviceId#' },
        sessionTime: { $ref: 'schemaSessionTime#' }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Результат выполнения создания игровой сессии',
        minProperties: 3,
        maxProperties: 3,
        properties: {
          errorCode: { type: 'integer' },
          errorMessage: { type: 'string' },
          resultData: { type: 'string' }
        }
      },
      400: {
        type: 'object',
        description: 'Неверный формат или отсутствует(-ют) параметры sessionId/deviceId/sessionTime',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaSessionPut = {
  schema: {
    tags: ['session'],
    description: 'Запускает игровую сессию подтвержденную QR кодом с телевизора',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'sessionId' ],
      properties: {
        sessionId: { $ref: 'schemaSessionId#' }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Результат выполнения запуска игровой сессии',
        minProperties: 3,
        maxProperties: 3,
        properties: {
          errorCode: { type: 'integer' },
          errorMessage: { type: 'string' },
          resultData: { type: 'string' }
        }
      },
      400: {
        type: 'object',
        description: 'Неверный формат или отсутствует параметр sessionId',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}

export const schemaSessionDelete = {
  schema: {
    tags: ['session'],
    description: 'Удаляет игровую сессию из очереди и выходит из нее если она активна',
    params: {
      type: 'object',
      additionalProperties: false,
      required: [ 'sessionId' ],
      properties: {
        sessionId: { $ref: 'schemaSessionId#' }
      }
    },
    response: {
      200: {
        type: 'object',
        description: 'Сессия удалена успешно, если сессия была активна то возвращает количество секунд до окончания, иначе 0',
      },
      400: {
        type: 'object',
        description: 'sessionId не соответствует формату / сессия не найдена',
        minProperties: 1,
        maxProperties: 1,
        properties: {
          message: { type: 'string' }
        }
      },
      401: {
        type: 'object',
        description: 'X-API-Key не найден или не соответствует',
      }
    },
    security: [{ apiKey: [] }]
  }
}