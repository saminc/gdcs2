'use strict'

import path from 'path'
import { readFileSync } from 'fs'
import { fileURLToPath } from 'url'
import Cors from '@fastify/cors'
import FastifyStatic from "@fastify/static"
import swagger from '@fastify/swagger'
import swaggerUI from '@fastify/swagger-ui'
import AutoLoad from '@fastify/autoload'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

global.appRoot = path.resolve(__dirname)

export const options = {
//  logger: envToLogger[process.env.APP_ENV],
  // http2: true,
  // https: {
  //   allowHTTP1: true,
  //   key: readFileSync(path.join(__dirname, '..', 'cert', 'key.pem')),
  //   cert: readFileSync(path.join(__dirname, '..', 'cert', 'cert.pem'))
  // }
}

export default async function (fastify, opts) {
  try {
    // load Cors support
    await fastify.register(Cors, { 
      origin: true,
      credentials: true
    })

    // configure serving static files
    fastify.register(FastifyStatic, {
      root: path.join(__dirname, 'routes/public'),
      prefix: '/public/'
    })

    // load and configure swagger
    await fastify.register(swagger, {
      openapi: {
        info: {
          title: 'GDCS JuniorBank API 2.0',
          description: 'Описание API системы управления игровыми устройствами (_Game Devices Control Systems_) для Junior Bank. Для авторизации используется `X-API-Key` в заголовке запроса.',
          version: '2.0.1',
          contact: {
            email: 'a.volovikov@kazintec.kz'
          }
        },
        servers: [{
          url: `http://10.15.192.11:${process.env.FASTIFY_PORT}`,
          description: 'Production Server'
        }],
        tags: [
          { name: 'session', description: 'Операции с указанной сессией' },
          { name: 'sessions', description: 'Операции со всеми сессиями' },
          { name: 'device', description: 'Операции с указанным устройством' },
          { name: 'devices', description: 'Операции со всеми устройствами' }
        ],
        components: {
          securitySchemes: {
            apiKey: {
              type: 'apiKey',
              name: 'X-API-KEY',
              in: 'header'
            }
          }
        }
      },
      hideUntagged: true,
      exposeRoute: true
    })
  
    // load swagger-ui
    await fastify.register(swaggerUI, {
      routePrefix: '/api/docs'
    })
    
    // load plugin modules
    await fastify.register(AutoLoad, {
      dir: path.join(__dirname, 'plugins'),
      options: Object.assign({}, opts)
    })

    // load API routes
    await fastify.register(AutoLoad, {
      dir: path.join(__dirname, 'routes/api'),
      options: Object.assign({ prefix: '/api' }, opts),
      autoHooks: true,
      cascadeHooks: true
    })

    // // load private service modules
    // await fastify.register(AutoLoad, {
    //   dir: path.join(__dirname, 'routes/private/'),
    //   options: Object.assign({}, opts)
    // })

    // fastify.ready().then(() => {
    //   fastify.log.info(`${process.env.APP_NAME} is ready!`)
    // //   // console.info(fastify.printRoutes())
    // })
  } catch (error) {
    fastify.log.error(`${process.env.APP_NAME} not started. Error: `, error)
  }
}
